# Solus Package for the Joplin note-taking and to-do application
This is a package for the note-taking and to-di Application Joplin

# Building this package
First you need to setup solbuild. How to setup solbuild is described here: https://getsol.us/articles/packaging/building-a-package/en/

After you have setup the package just build it via 

    make build

# Installing this package
Afterwards you can install the generated *.eopkg via `sudo eopkg install Joplin-*.eopk`